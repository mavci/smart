<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$ldr_data = \App\SensorData::where('sensor', 'ldr')->orderBy('created_at', 'desc')->get();
	$inf_data = \App\SensorData::where('sensor', 'LIKE', 'inf%')->orderBy('created_at', 'desc')->get();

    return view('index', compact('ldr_data', 'inf_data'));
});
