<?php

use Illuminate\Http\Request;

Route::get('data', function(Request $request) {
	if ($request->token !== env('TOKEN')) {
		abort(403);
	}

	\App\SensorData::create($request->only('sensor', 'data'));

	if (($count = \App\SensorData::where('sensor', 'LIKE', 'inf%')->where('created_at', '>', date('Y-m-d H:i:s', time() - (60)))->count()) > 5) {
		Cache::remember('mail_grace_period', 1, function() use($count) {
			Mail::to(env('CONTACT_MAIL'))->send(new \App\Mail\MotionDetected($count));
			return 1;
		});
	}
});

Route::get('clear', function(Request $request) {
	if ($request->token !== env('TOKEN')) {
		abort(403);
	}

	\App\SensorData::truncate();
});

Route::get('test', function() {

});