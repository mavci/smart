<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SensorData extends Model
{
	protected $table = 'sensor_data';

	public $fillable = ['sensor', 'data'];
}
