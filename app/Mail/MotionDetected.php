<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Contracts\Queue\ShouldQueue;

class MotionDetected extends Mailable
{
	public $motions;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($motions)
    {
        $this->motions = $motions;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.motion')->with(['motions' => $this->motions]);
    }
}
