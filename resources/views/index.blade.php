<!doctype html>
<html lang="tr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Smart Lighting</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col">
				<h1>Smart Lighting</h1>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<h2>LDR</h2>
				<div id="ldr_chart_div"></div>
				<table class="table table-striped">
					<thead>
					<tr>
						<th>Date</th>
						<th>Data</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($ldr_data as $row)
						<tr>
							<td>{{ $row->created_at }}</td>
							<td>{{ $row->data }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
			<div class="col">
				<h2>Motion</h2>
				<table class="table table-striped">
					<thead>
					<tr>
						<th>Date</th>
						<th>Sensor</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($inf_data as $row)
						<tr>
							<td>{{ $row->created_at }}</td>
							<td>{{ $row->sensor }}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script>
		google.charts.load('current', {packages: ['corechart', 'line']});
		google.charts.setOnLoadCallback(drawLineColors);

		function drawLineColors() {
			var data = new google.visualization.DataTable();
			data.addColumn('datetime', 'Date');
			data.addColumn('number', 'Data');

			data.addRows([
				@foreach ($ldr_data as $row)
				[new Date({{ $row->created_at->format('Y, m, d, H, i, s') }}), {{ $row->data }}],
				@endforeach
			]);

			var options = {
				hAxis: {
					title: 'Date'
				},
				vAxis: {
					title: 'LDR'
				},
				colors: ['#a52714', '#097138']
			};

			var chart = new google.visualization.LineChart(document.getElementById('ldr_chart_div'));
			chart.draw(data, options);
		}
	</script>
</body>
</html>