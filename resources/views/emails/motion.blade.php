@component('mail::message')
# Motion Detected

{{ $motions }} motions detected last 1 minute.

@component('mail::button', ['url' => 'http://smart.febir.org'])
Click for more info
@endcomponent

Thanks<br>
@endcomponent
